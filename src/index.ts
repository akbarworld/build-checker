import { APIClient } from "bitbucket/src/plugins/register-endpoints/types";

const commandLineArgs = require('command-line-args');
const { Bitbucket } = require('bitbucket');
export class BitbucketBuildChecker{


    private client: APIClient = undefined;
    private workspace: string = undefined;
    private repo: string = undefined;
    constructor(
        repoLocation: {
            workspace: string,
            repo: string
        }
    ){
        this.repo = repoLocation.repo;
        this.workspace = repoLocation.workspace;
        this.client = new Bitbucket(this.loadConfig(repoLocation))
    }

    loadConfig = (location: {workspace: string, repo: string}, auth?: {username: string, password: string}) => {
   
        const clientOptions: any = {
            baseUrl: 'https://api.bitbucket.org/2.0'
          }

        const credentials: any = {};
        
        if(process.env.BITBUCKET_USERNAME){
            credentials.username = process.env.BITBUCKET_USERNAME;
        }

        if(process.env.BITBUCKET_PASSWORD){
            credentials.password = process.env.BITBUCKET_PASSWORD;
        }

        if(process.env.BITBUCKET_USERNAME && process.env.BITBUCKET_PASSWORD){
            clientOptions.auth = credentials;
        }

        if (auth) {
            clientOptions.auth = {
                username: auth.username,
                password: auth.password
            }
        }
        console.log(clientOptions);
        return clientOptions;
    }

    getBuildStatus(commit: string){
        console.log('Checking status for commit ' + commit + ' in repo slug ' + this.repo + ' located in workspace ' + this.workspace);
        this.client.commitstatuses.list({
            node: commit,
            repo_slug: this.repo,
            workspace: this.workspace

        }).then(({data, headers}: {data: any, headers: any}) => {
            let code: number = 1;
            switch(data.values[0].state){
                case 'SUCCESSFUL':
                    code = 0;
                break;
                case 'INPROGRESS':
                    code = 2;
                break;
                case 'FAILED':
                case 'STOPPED':
                default:
                    code = 1;
                break;
            }
            console.log("Status found : "+data.values[0].state+" ! Exiting with code " + code);
            process.exit(code);
        }).catch((e: Error) => {
            console.error('An error happened. See stacktrace below :');
            console.trace(e)
            process.exit(1);
        })
    }
}

const optionsDefinitions = [
    {
        alias: 'w',
        help: 'The workspace to work on',
        name: 'workspace'
    },
    {
        alias: 'r',
        help: 'The repo slug from which the commit comes from',
        name: 'repo'
    },
    {
        alias: 'c',
        help: 'The commit for which to check the build',
        name: 'commit'
    }
]

const cliOptions = commandLineArgs(optionsDefinitions);

const checkBuildStatus = () => {

    if(!cliOptions.workspace) {
        console.error('No workspace found. You must specify the workspace name. Exiting...');
        process.exit(404);
    }

    if(!cliOptions.commit) {
        console.error('No commit found. You must specify the commit reference. Exiting...');
        process.exit(404);
    }
    if(!cliOptions.repo) {
        console.error('No repo found. You must specify the repo slug. Exiting...');
        process.exit(404);
    }

    const bbBC = new BitbucketBuildChecker({
        workspace: cliOptions.workspace,
        repo: cliOptions.repo
    });

    return bbBC.getBuildStatus(cliOptions.commit);
}

checkBuildStatus();