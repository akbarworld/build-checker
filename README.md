# Bitbucket simple build checker

A simple micro-library to check for a successful build for a repo commit through bitbucket API.

# What does this project stands for and what not ? 

This project has been developed to fill the need of checking build on repos declared as submodules in detached orchestration repo. 

To ensure orchestration repo it will process its workflow only with releases fullfiling unitary integrity, this script is run for each repo in a job amongst any other job. 

If one of the submodules provides a commit with a failed build as last build, the script will exit with error returning `1` which will declare the orchestration workflow as a failure and abort any further job. If a job is still pending or running, script will exit with error returning `2`. In both cases, this will stop the CI workflow. 

The script is exported as an exportable single-file node application through the help of `ncc`.

If you intend to reuse this project and don't want the micro-script to rely on installed node modules, once cloned and installed the current dependencies you can create the export through the current command : 

`node_modules/.bin/ncc src/index.ts`

The single file application will be compiled to `dist` folder. Then you can move the file anywhere you need it. 

